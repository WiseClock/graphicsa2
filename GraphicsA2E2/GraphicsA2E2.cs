﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace GraphicsA2E2
{
    public partial class GraphicsA2E2 : Form
    {

        private enum bgColor
        {
            BlackWhite,
            WhiteBlack
        }

        private bgColor backGround = bgColor.BlackWhite;

        public GraphicsA2E2()
        {
            InitializeComponent();

            Text = "C4560: Assignment 2, Exercise 2 (Carl Kuang, 4B, 2014)";
            ResizeRedraw = true;
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            Graphics g = e.Graphics;

            g.SmoothingMode = SmoothingMode.HighQuality;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;

            float cx = ClientSize.Width;
            float cy = ClientSize.Height;
            float mx = cx / 2;

            // s and g
            float square = 10;
            float gap = 5;

            // Brushes
            SolidBrush brushBlack = new SolidBrush(Color.Black);
            SolidBrush brushWhite = new SolidBrush(Color.White);
            SolidBrush brushSquare = new SolidBrush(Color.FromArgb(190, 255, 0));

            // Draw background
            switch (backGround)
            {
                case bgColor.BlackWhite:
                    g.FillRectangle(brushBlack, 0, 0, cx, cy);
                    g.FillRectangle(brushWhite, mx, 0, mx, cy);
                    break;
                case bgColor.WhiteBlack:
                    g.FillRectangle(brushWhite, 0, 0, cx, cy);
                    g.FillRectangle(brushBlack, mx, 0, mx, cy);
                    break;
            }

            // Draw squares
            float uly = square + gap;
            float ulxl = cx / 2 - square;
            float ulxr = cx / 2;
            while (uly <= cy - (2 * square + gap))
            {
                while (ulxl >= 2 * square + gap)
                {
                    g.FillRectangle(brushSquare, ulxl, uly, square, square);
                    ulxl -= square + gap;
                }
                while (ulxr <= cx - (2 * square + gap))
                {
                    g.FillRectangle(brushSquare, ulxr, uly, square, square);
                    ulxr += square + gap;
                }
                ulxl = cx / 2 - square;
                ulxr = cx / 2;
                uly += square + gap;
            }
        }

        private void GraphicsA2E2_Click(object sender, EventArgs e)
        {
            backGround = (backGround == bgColor.BlackWhite) ? bgColor.WhiteBlack : bgColor.BlackWhite;
            Refresh();
        }
    }
}
