﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace GraphicsA2
{
    public partial class GraphicsA2E1 : Form
    {
        public GraphicsA2E1()
        {
            InitializeComponent();

            Text = "C4560: Assignment 2, Exercise 1 (Carl Kuang, 4B, 2014)";
            BackColor = Color.Black;
            ResizeRedraw = true;
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            Graphics g = e.Graphics;

            g.SmoothingMode = SmoothingMode.HighQuality;
            g.PixelOffsetMode = PixelOffsetMode.HighQuality;

            float cx = ClientSize.Width;
            float cy = ClientSize.Height;
            float mx = cx / 2;
            float my = cy / 2;
            float s = (cx > cy) ? cy : cx;
            float radiusS = s / 12;
            float radiusL = s / 6;
            float diameterS = radiusS * 2;
            float diameterL = radiusL * 2;

            // Red
            SolidBrush brushRed = new SolidBrush(Color.FromArgb(255, 0, 0));
            // Green
            SolidBrush brushGreen = new SolidBrush(Color.FromArgb(0, 192, 0));
            // Blue
            SolidBrush brushBlue = new SolidBrush(Color.FromArgb(0, 0, 255));
            // Yellow
            SolidBrush brushYellow = new SolidBrush(Color.FromArgb(255, 255, 0));

            // 4 large circles
            g.FillEllipse(brushBlue, mx - 5 * radiusS, my - 5 * radiusS, diameterL, diameterL);
            g.FillEllipse(brushBlue, mx - 5 * radiusS, my + radiusS, diameterL, diameterL);
            g.FillEllipse(brushYellow, mx + radiusS, my - 5 * radiusS, diameterL, diameterL);
            g.FillEllipse(brushYellow, mx + radiusS, my + radiusS, diameterL, diameterL);

            // 4 Small circles
            g.FillEllipse(brushGreen, mx - 2 * radiusL, my - 2 * radiusL, diameterS, diameterS);
            g.FillEllipse(brushRed, mx - 2 * radiusL, my + radiusL, diameterS, diameterS);
            g.FillEllipse(brushGreen, mx + radiusL, my - 2 * radiusL, diameterS, diameterS);
            g.FillEllipse(brushRed, mx + radiusL, my + radiusL, diameterS, diameterS);
        }
    }
}
